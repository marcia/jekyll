# Virtua Creative Websites

## Testing Multiple Jekyll Themes on GitLab Pages

### This website is based on the Theme Carte Noire.

This Jekyll Theme can be founf on [GitHub](https://github.com/jacobtomlinson/carte-noire).

[Seeting up Carte Noire with GitLab Pages](https://virtuacreative.gitlab.io/jekyll/2016/jekyll-carte-noire-gl-pages.markdown/).
