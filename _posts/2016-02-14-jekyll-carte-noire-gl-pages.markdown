---
layout:     post
title:      Carte Noire
date:       2016-02-14 20:35:55
author:     Marcia Ramos
author_url: https://plus.google.com/+MarciaDiasRamosMD/
summary:    Learn how to set up Jekyll v2.x Theme, Carte Noire, on GitLab Pages.
categories: jekyll
thumbnail:  jekyll
tags:
 - Jekyll
 - Theme
 - GitLab
 - GitLab Pages
---

# [Carte Noire][]: [Jekyll][] Theme 

This beautifull theme was made by [Jacob Tomlinson](http://www.twitter.com/_jacobtomlinson) and can be found at [Jekyll Themes](http://themes.jekyllrc.org/carte-noire/) and downloaded from [GitHub](https://github.com/jacobtomlinson/carte-noire) directly.

This website is based on [Carte Noire][] :smiley: 

<!-- ## Why GitLab Pages?

There are a lot of features and advantages on using GitLab Pages. Please read the article [How to Publish your Website on GitLab]().
 -->

## Setting up Carte Noire to GitLab Pages

In order to host this theme on [GitLab](http://doc.gitlab.com/ee/pages/README.html) please follow the instructions:

-1. Configure GitLab Pages `.gitlab-ci.yml` like the following code block:

{% highlight ruby %}
image: ruby:2.1

before_script:
  - apt-get update -qy  # => will have nodejs available
  - apt-get install -y nodejs  # => install nodejs - required by execjs

pages:
  script:
  - gem install bundler
  - gem install nokogiri -v '1.6.7.2' -- --use-system-libraries # avoid nokogiri error
  - bundle config build.nokogiri --use-system-libraries
  - gem install execjs 
  - gem install rake 
  - bundle install --path ~/bundle
  - bundle exec jekyll build -d public/  # => serve Jekyll with Bundler
  artifacts:
    paths:
    - public
{% endhighlight %}

Serving Jekyll with Bundler will make sure that all `gems` and their dependencies are installed to the project.

-2. Configure your `Gemfile`:

{% highlight ruby %}
source "https://rubygems.org"

gem 'jekyll', '2.5.3'
gem 'redcarpet'
gem 'liquid', '2.6.1'
{% endhighlight %}

[Carte Noire - v.0.2.0](https://github.com/jacobtomlinson/carte-noire/releases/tag/v0.2.0) is a theme based on [Jekyll][] v.2.x, when `redcarpet` and `pygments` were `markdown` and `highlighter` by default. But [Carte Noire][] has been updated to Jekyll v.3.0.3, which can be used freely on GitLab! If you want to use this Jekyll version, it's just require it on `Gemfile`, by replacing  `gem 'jekyll', '2.5.3'` for `gem 'jekyll', '3.0.3'` and `gem 'liquid', '2.6.1'` for `gem 'liquid'`!

It was necessary to require Liquid 2.6.1 to work with Jekyll 2.5.3, as some _Liquid Exception_ errors failed building.

-3. Add both `.gitlab-ci.yml` and `Gemfile` to your project's root. GitLab Pages will build your website for you now! :smile:

### About _config.yml

Keep it that way:

{% highlight ruby %}
# some settings
markdown: redcarpet
redcarpet:
  extensions: ["no_intra_emphasis", "fenced_code_blocks", "autolink", "strikethrough", "superscript"]
# some settings
{% endhighlight %}

You don't need to do anything to your `_config.yml` file. Just add your own data and you'll be good to go. Good luck!

------

## Extra

### Documentation
- [NodeJS](http://doc.gitlab.com/ee/ci/examples/test-and-deploy-ruby-application-to-heroku.html) on GitLab
- [.gitlab-ci.yml](http://doc.gitlab.com/ee/ci/yaml/README.html) documentation
- [GitLab Pages](http://doc.gitlab.com/ee/pages/README.html) setup

### Oficial Information

The following information was given by the author, and can be found on the [readme](https://github.com/jacobtomlinson/carte-noire#carte-noire) file in his repository.

#### Made by

[Jacob Tomlinson](http://www.twitter.com/_jacobtomlinson)

#### Tools and Libraries
The following tools and libraries are used in this theme

#### JavaScript
 * [jQuery](http://jquery.com/)
 * [MMenu](http://mmenu.frebsite.nl/)
 * [HighlightJS](https://highlightjs.org/)
 * [Simple Jekyll Search](https://github.com/christian-fei/Simple-Jekyll-Search)

#### CSS
 * [Bootstrap](http://getbootstrap.com/)
 * [Font Awesome](http://fortawesome.github.io/Font-Awesome/)

#### Social
 * [AddThis](http://www.addthis.com/)
 * [Disqus](https://disqus.com/)

#### Other
 * [Real Favicon Generator](http://realfavicongenerator.net/)
 * [Google Analytics](http://www.google.com/analytics/)

#### License
The jekyll theme, HTML, CSS and JavaScript is licensed under GPLv3 (unless stated otherwise in the file).

[Carte Noire]: http://themes.jekyllrc.org/carte-noire/
[Jekyll]: http://jekyllrb.com/